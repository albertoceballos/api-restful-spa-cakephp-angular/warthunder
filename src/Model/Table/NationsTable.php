<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Nations Model
 *
 * @method \App\Model\Entity\Nation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Nation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Nation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Nation|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Nation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Nation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Nation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Nation findOrCreate($search, callable $callback = null, $options = [])
 */
class NationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('nations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name_en')
            ->maxLength('name_en', 200)
            ->requirePresence('name_en', 'create')
            ->notEmptyString('name_en');

        $validator
            ->scalar('name_sp')
            ->maxLength('name_sp', 200)
            ->allowEmptyString('name_sp');

        $validator
            ->scalar('flag')
            ->maxLength('flag', 350)
            ->allowEmptyString('flag');

        return $validator;
    }
}
