<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Nation Entity
 *
 * @property int $id
 * @property string $name_en
 * @property string|null $name_sp
 * @property string|null $flag
 */
class Nation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name_en' => true,
        'name_sp' => true,
        'flag' => true,
    ];
}
